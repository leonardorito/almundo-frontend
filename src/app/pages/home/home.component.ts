import { Component, OnInit } from '@angular/core';
import { HotelService } from '../../services/hotel';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [HotelService]
})
export class HomeComponent implements OnInit {

  hotels: any[] = [];

  public formSearch: FormGroup;

  stars = [];

  constructor(
    private _hotelService: HotelService,
    private _fb: FormBuilder
  ) { }

  counter(i: number) {
    return new Array(i);
  }

  updateCheckedOptions(option, event) {
    if (event.target.checked) {
      if (parseInt(event.target.value)) {
        this.stars.push(parseInt(event.target.value));
      } else {
        this.stars.push(event.target.value);
      }
    } else {
      const index = this.stars.indexOf(parseInt(event.target.value));
      this.stars.splice(index, 1);
    }
  }

  getHotels() {
    this._hotelService.getHotels().subscribe(data => {
      this.hotels = data["data"]["hotels"];
    }, err => console.log(err));
  }

  search() {
    this._hotelService.getFilterHotels(this.formSearch.value.name, this.stars).subscribe(data => {
      this.hotels = data['data']['hotels'];
    }, err=> console.log(err))
  }

  ngOnInit() {
    this.formSearch = this._fb.group({
      name: ['', [Validators.required]],
      stars: ['', [
        Validators.required,
      ]]
    });
    this.getHotels();
  }

}
