# Almundo

## Instrucciones para correr el proyecto en local 

IMPORTANTE: Debe tener instalado el angular cli global. Puede hacerlo corriendo npm install -g @angular/cli

Luego de clonar el proyecto, ir desde el terminal hasta la carpeta del mismo y correr 'npm install'

Al finalizar npm install, se debe correr el comando 'ng serve -o', el cual montará el ambiente de desarrollo en local, es recomendable tener el puerto 4200 desocupado.

En caso de que el anterior comando falle, correr simplemente 'ng serve' e ir manualmente en el navegador a http://localhost:4200/