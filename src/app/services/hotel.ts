import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { ENDPOINT } from '../config/api';

@Injectable()
export class HotelService {
  constructor(
    private httpClient: HttpClient
  ) {
  }

  public getHotels() {
    return this.httpClient.get<any>(`${ENDPOINT}gethotels`);
  }

  public getFilterHotels(name, stars) {
    return this.httpClient.get<any>(`${ENDPOINT}getfilterhotels?name=${name}&stars=[${stars}]`);
  }
}
